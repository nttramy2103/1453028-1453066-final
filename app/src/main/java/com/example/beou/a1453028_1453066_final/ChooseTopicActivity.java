package com.example.beou.a1453028_1453066_final;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beou.a1453028_1453066_final.component.GeneralClass;


public class ChooseTopicActivity extends Activity {

    private LayoutInflater layoutInflater;
    private GridView mGridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_topic);

        mGridView = (GridView) findViewById(R.id.my_grid_view);
        layoutInflater = LayoutInflater.from(getApplicationContext());
        mGridView.setAdapter(new MyAdapter());
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> adapterViews, View pView, int position, long id) {

//                TextView textView = (TextView) pView.findViewById(R.id.my_text_view);
////                if (textView != null) {
////
////                    //Xử lý vị trí ở đây
////
////                    textView.setText("Changed : Position = " + position );
////                } else {
////
////                }
                startActivity(new Intent(getApplication(), GeneralClass.practiceLayoutActivity[position]));
            }
        });

    }


    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {

            return GeneralClass.topic.length;
        }
        @Override
        public Object getItem(int arg0) {
            return arg0;
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = layoutInflater.inflate(R.layout.gridview_item,null);

                //Set ảnh
                ImageView imageView = (ImageView) convertView.findViewById(R.id.my_image_view);
                imageView.setImageResource(GeneralClass.imgTopic[position]);
                //Set text
                TextView txt = (TextView) convertView.findViewById(R.id.my_text_view);
                txt.setText(GeneralClass.topic[position]);
            }
            return convertView;
        }

    }}


