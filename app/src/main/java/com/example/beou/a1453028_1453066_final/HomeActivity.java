package com.example.beou.a1453028_1453066_final;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.example.beou.a1453028_1453066_final.component.Room;
import com.example.beou.a1453028_1453066_final.waitingroomlist.RoomListActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HomeActivity extends Activity {
    FirebaseDatabase root= FirebaseDatabase.getInstance();
    DatabaseReference roomDB= root.getReference(GeneralClass.ROOM_CHILD);
    Button btnCreateRoom;
    Button btnListRoom;
    Button btnPractice;
    Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        addControl();

        btnCreateRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Room room = new Room();
                room.createRoom(HomeActivity.this);
//                Toast.makeText(HomeActivity.this, room.toString(), Toast.LENGTH_SHORT).show();
//                if (room.getName().length()!=0) {
//                    Toast.makeText(HomeActivity.this, "lala", Toast.LENGTH_SHORT).show();
//                    DatabaseReference tmp = roomDB.push();
//                    room.setRoomID(tmp.getKey());
//                    tmp.setValue(room);
//                    Intent roomListIntent = new Intent(getApplication(), RoomListActivity.class);
//                    //Bundle extras = mIntent.getExtras();
//                    //roomListIntent.putExtra("room", room);
//                    startActivity(roomListIntent);
//                }
//                else{
//                    Toast.makeText(HomeActivity.this, "huhu", Toast.LENGTH_SHORT).show();
//                }
            }
        });
        btnListRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent(getApplication(),RoomListActivity.class));
            }
        });
        btnPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent(getApplication(),ChooseTopicActivity.class));
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity( new Intent(getApplication(),MainActivity.class));
            }
        });
    }

    private void addControl() {
        btnCreateRoom = (Button) findViewById(R.id.btnCreateRoom);
        btnListRoom = (Button) findViewById(R.id.btnListRoom);
        btnPractice = (Button) findViewById(R.id.btnPractice);
        btnExit = (Button) findViewById(R.id.btnExit);

    }
}
