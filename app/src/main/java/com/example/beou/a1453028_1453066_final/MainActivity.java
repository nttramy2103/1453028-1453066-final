package com.example.beou.a1453028_1453066_final;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.beou.a1453028_1453066_final.useractivity.LogInActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends Activity {
    //private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Intent login = new Intent(this,NumberQuizActivity1.class);
//        startActivity(login);
//        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            startActivity(new Intent(this, HomeActivity.class));
            Toast.makeText(this, "Xin chào "+ user.getDisplayName(), Toast.LENGTH_SHORT).show();
        } else {
            startActivity(new Intent(this, LogInActivity.class));

        }

        finish();
//        if (mAuth.getCurrentUser() == false) {
//            startActivity(new Intent(this, LogInActivity.class));
//            finish();
//        }


//        startActivity(new Intent(this, LogInActivity.class));
//        finish();
    }
}
