package com.example.beou.a1453028_1453066_final;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.example.beou.a1453028_1453066_final.waitingroom.ChatAdapter;
import com.example.beou.a1453028_1453066_final.waitingroom.ChatItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {
    ImageView imageavatar2,imageavatar1;
    Button btnSend, btnBack2Home;
    EditText txtComment;
    ListView listViewChat,listViewMyUser, listViewOpUser;
    DatabaseReference curRoomDB;
    DatabaseReference mesDB;
    DatabaseReference userNodeDB = FirebaseDatabase.getInstance().getReference(GeneralClass.USER_CHILD);
    DatabaseReference quizDB = null;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    ArrayList<ChatItem> chatItems = new ArrayList<ChatItem>();
    ArrayList<ResultItem> resultItems = new ArrayList<ResultItem>();
    ArrayList<ResultItem> opResultItems = new ArrayList<ResultItem>();
    String opUserID = null;
    String cmt;

    ArrayList<Integer> ranQues = new ArrayList<Integer>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        addControl();

        final ChatAdapter chatAdapter = new ChatAdapter(this,R.layout.myuser_item,chatItems);
        listViewChat.setAdapter(chatAdapter);

        final ResultAdapter resultAdapter = new ResultAdapter(this, R.layout.result_item,resultItems);
        listViewMyUser.setAdapter(resultAdapter);


        final ResultAdapter opResultAdapter = new ResultAdapter(this, R.layout.result_item,opResultItems);
        listViewOpUser.setAdapter(opResultAdapter);

        final String curRoom = getIntent().getStringExtra("roomID");
        //Toast.makeText(this, curRoom, Toast.LENGTH_SHORT).show();
        curRoomDB = FirebaseDatabase.getInstance().getReference(GeneralClass.PLAY_ROOM_CHILD).child(curRoom);
        //FirebaseDatabase.getInstance().getReference(GeneralClass.ROOM_CHILD).child(curRoom).child("numOfUser").addValueEventListener(new ValueEventListener() {
        setUserAva();
        btnBack2Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralClass.opUserID = null;
                startActivity(new Intent(getApplication(),HomeActivity.class));
                finish();
            }
        });
        //chat feature ------------------------------------------------------------
        mesDB = curRoomDB.child(GeneralClass.MESSAGES_CHILD);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cmt=txtComment.getText().toString();
                if (cmt.length()!= 0){
                    txtComment.setText("");
                    mesDB.push().setValue(new ChatItem(user.getUid(),cmt));;

                }
            }
        });

        //hien tin nhan len tren list view
        mesDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                chatItems.add(dataSnapshot.getValue(ChatItem.class));
                //rooms.add((Room) dataSnapshot.getValue());
                chatAdapter.notifyDataSetChanged();
                listViewChat.setSelection(listViewChat.getAdapter().getCount()-1);
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        //-------------------------------------------------------------------------

        //hiện kết quả----------------
        curRoomDB.child(GeneralClass.RECORD_CHILD).child(user.getUid())
                .addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                resultItems.add(dataSnapshot.getValue(ResultItem.class));
                //rooms.add((Room) dataSnapshot.getValue());
                resultAdapter.notifyDataSetChanged();

            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        if(GeneralClass.opUserID!=null) {
            curRoomDB.child(GeneralClass.RECORD_CHILD).child(GeneralClass.opUserID)
                    .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            //Toast.makeText(ResultActivity.this, opUserID, Toast.LENGTH_SHORT).show();
                            opResultItems.add(dataSnapshot.getValue(ResultItem.class));
                            //rooms.add((Room) dataSnapshot.getValue());
                            opResultAdapter.notifyDataSetChanged();

                        }
                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {}
                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });
        }
        //-------------------------------------------------------------------------

    }


    private void addControl() {
        txtComment = (EditText) findViewById(R.id.txtComment);
        btnSend = (Button) findViewById(R.id.btnSend);
        btnBack2Home = (Button) findViewById(R.id.btnBack2Home);
        listViewChat = (ListView) findViewById(R.id.listViewChat);
        listViewMyUser = (ListView) findViewById(R.id.listViewMyUser);
        listViewOpUser = (ListView) findViewById(R.id.listViewOpUser);
        imageavatar2 = (ImageView) findViewById(R.id.imageavatar2);
        imageavatar1 = (ImageView) findViewById(R.id.imageavatar1);
    }
    private void setUserAva(){
        userNodeDB.child(user.getUid()).child("photoUrl").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Picasso.with(getApplication())
                        .load(dataSnapshot.getValue().toString())
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error)
                        .into(imageavatar1);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        //set ava cho opUser
        if (GeneralClass.opUserID!=null) {
            userNodeDB.child(GeneralClass.opUserID).child("photoUrl").addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Picasso.with(getApplication())
                            .load(dataSnapshot.getValue().toString())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error)
                            .into(imageavatar2);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }

    }

}
