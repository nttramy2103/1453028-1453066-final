package com.example.beou.a1453028_1453066_final;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by BeoU on 12/16/2016.
 */

public class ResultAdapter extends ArrayAdapter<ResultItem> {
    Context context;
    ArrayList<ResultItem> resultItems;
    public ResultAdapter(Context context, int resource, ArrayList<ResultItem> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resultItems = objects;
    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.result_item,parent,false);
        }
        ResultItem resultItem = resultItems.get(position);
        TextView txtSecond = (TextView) convertView.findViewById(R.id.txtSecond);
        TextView txtScore = (TextView) convertView.findViewById(R.id.txtScore);

        int score = resultItem.getScore();
        txtScore.setText(score+"");
        txtSecond.setText(resultItem.getSecond()+"s");
        String color = "#000000";
        if (score==0){

        }
        else if (score>0){
            color = "#009900";
        }
        else{
            color = "#FF4C4C";
        }

        txtScore.setTextColor(Color.parseColor(color));
        txtSecond.setTextColor(Color.parseColor(color));


        return convertView;
    }
}
