package com.example.beou.a1453028_1453066_final;

/**
 * Created by BeoU on 12/15/2016.
 */

public class ResultItem {
    int second;
    int score;

    public ResultItem() {
    }

    public ResultItem(int second, int score) {
        this.second = second;
        this.score = score;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
