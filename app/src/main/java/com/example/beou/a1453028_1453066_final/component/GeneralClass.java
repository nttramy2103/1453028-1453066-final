package com.example.beou.a1453028_1453066_final.component;

import android.app.Activity;
import android.view.WindowManager;
import android.widget.EditText;

import com.example.beou.a1453028_1453066_final.playactivity.NumberQuizActivity1;
import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.playactivity.ShapeQuizActivity1;


/**
 * Created by BeoU on 11/30/2016.
 */

public class GeneralClass {
    public final static String USER_CHILD = "Users";
    public final static String ROOM_CHILD = "Rooms";
    public final static String PLAY_ROOM_CHILD = "PlayRooms";
    public final static String QUIZ_CHILD = "Quiz"; // node chứa câu hỏi hiện tại của user khi chơi 2 người
    public final static String[] topic = {
            "Trắc nghiệm chọn hình",
            "Nhập số",
    };

    public final static int[] imgTopic ={
            R.drawable.shape,
            R.drawable.numbers
    };
    public final static String PRACTICE_CHILD = "PracticeQuiz";// node chứa câu hỏi hiện tại của user khi chơi 2 người
    public final static String NUMBER_CHILD = "Number";
    public final static String SHAPE_CHILD = "Shape";
    public final static String IMAGE_CHILD = "Image";
    public final static String QUES_CHILD = "Question";
    public final static String ANSWER_CHILD = "Answer";
    public final static String MESSAGES_CHILD = "Messages";
    public final static String READY_CHILD = "Ready";
    public final static String RANDOMARR_CHILD = "RandomArray";
    public final static String RECORD_CHILD = "Record";
    public final static String TOTALSCORE_CHILD = "TotalScore";
    public static String opUserID =null;

    public final static  int MAX_SEC = 300;// 5 phút

    public static String position  = "1"; // nếu là ng tạo phòng thì position 1
                                            //ng vào phòng position =2
    //public static String curRoom = "";


    public final static int numOfRequireQues = 5;

    public final static Class practiceLayoutActivity[]={ShapeQuizActivity1.class, NumberQuizActivity1.class};
    public final static int  playLayoutID[]={-1, R.layout.activity_number_1};
    public static void informErrorEditText(Activity activity, EditText editText, String error){
        editText.setFocusable(true);
        if(editText.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        editText.setError(error);

    }

}
