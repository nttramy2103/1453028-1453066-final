package com.example.beou.a1453028_1453066_final.component;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.beou.a1453028_1453066_final.HomeActivity;
import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.waitingroom.WaitingRoomActivity;
import com.example.beou.a1453028_1453066_final.waitingroomlist.RoomListActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by BeoU on 12/1/2016.
 */

public class Room{
    String roomID=null;
    String name=null;
    String topic=null;
    int numOfUser = 1;
    String password = null;
    boolean isWaiting = true;

    public String getRoomID() {
        return roomID;
    }

    public void setRoomID(String roomID) {
        this.roomID = roomID;
    }

    public Room(String roomID, String name, String topic) {
        this.roomID = roomID;
        this.name = name;
        this.topic = topic;
        this.numOfUser = numOfUser;
    }

    public Room(String roomID, String name, String topic, String password) {
        this.roomID = roomID;
        this.name = name;
        this.topic = topic;
        this.numOfUser = numOfUser;
        this.password = password;
    }

    public Room(String roomID, String name, String topic, int numOfUser, String password) {
        this.roomID = roomID;
        this.name = name;
        this.topic = topic;
        this.numOfUser = numOfUser;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Room() {
    }

    public String getName() {
        return name;
    }

    public String getTopic() {
        return topic;
    }

    public int getNumOfUser() {
        return numOfUser;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setNumOfUser(int numOfUser) {
        this.numOfUser = numOfUser;
    }
    public void createRoom(Context context){
        EditText txtDlgName;
        EditText txtDlgPassword;
        Spinner spinner;
        CheckBox checkbox;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View layout = LayoutInflater.from(context).inflate(R.layout.dialog_createroom,null);
        builder.setView(layout);

        builder.setPositiveButton("Tạo phòng", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

            }
        })
                .setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        //
        AlertDialog dialog = builder.create();
        dialog.show();
        //

        txtDlgName = (EditText)layout.findViewById(R.id.txtroomname);
        txtDlgPassword = (EditText) layout.findViewById(R.id.txtpassword);


        checkbox = (CheckBox)layout.findViewById(R.id.checkBox);
        spinner = (Spinner)layout.findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context
                                        ,android.R.layout.simple_spinner_item
                                        ,new ArrayList<String>(Arrays.asList(GeneralClass.topic)));

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        final EditText finalTxtDlgPassword = txtDlgPassword;
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //Kiểm tra có pass hay không
                if(isChecked)
                {
                    finalTxtDlgPassword.setEnabled(true);
                    finalTxtDlgPassword.setVisibility(View.VISIBLE);
                }
                else
                {
                    finalTxtDlgPassword.setVisibility(View.INVISIBLE);
                    //finalTxtDlgPassword.setEnabled(false);
                }
            }
        });
        Button theButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        theButton.setOnClickListener(new CustomListener(dialog,txtDlgPassword,txtDlgName,checkbox,(Activity)context,spinner));
    }
    class CustomListener implements View.OnClickListener {
        FirebaseDatabase root= FirebaseDatabase.getInstance();
        DatabaseReference roomDB= root.getReference(GeneralClass.ROOM_CHILD);
        private final Dialog dialog;
        private EditText txtDlgPassword;
        private EditText txtDlgName;
        private CheckBox checkbox;
        private Activity activity;
        private Spinner spinner;

        public CustomListener(Dialog dialog, EditText txtDlgPassword, EditText txtDlgName, CheckBox checkbox, Activity activity, Spinner spinner) {
            this.dialog = dialog;
            this.txtDlgPassword = txtDlgPassword;
            this.txtDlgName = txtDlgName;
            this.checkbox = checkbox;
            this.activity = activity;
            this.spinner = spinner;
        }

        @Override
        public void onClick(View v) {
            boolean correct = true;
            name = txtDlgName.getText().toString();
            password = txtDlgPassword.getText().toString();
            //Toast.makeText(MainActivity.this, "aa", Toast.LENGTH_SHORT).show();
            if(checkbox.isChecked()) {
                //Lấy pass
                if (password.length() == 0){
                    correct = false;
                    txtDlgPassword.setFocusable(true);
                    if(txtDlgPassword.requestFocus()) {
                        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                    txtDlgPassword.setError("Cần nhập password");
                }

            }

            if (name.length() == 0){
                correct = false;
                txtDlgName.setFocusable(true);
                if(txtDlgName.requestFocus()) {
                    activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
                txtDlgName.setError("Cần nhập tên");
            }

            if (correct == true) {
                //Lấy chủ đề
                dialog.dismiss();
//                txtSpinner.setText(String.valueOf(spinner.getSelectedItem()));
//                // If you want to close the dialog, uncomment the line below
//                txtName.setText(txtDlgName.getText().toString());
//                txtName.setText(txtDlgName.getText().toString());
//                txtPassword.setText(txtDlgPassword.getText().toString());
                topic = String.valueOf(spinner.getSelectedItem());
                roomID = roomDB.push().getKey();
                //curRoom = roomID;
                //roomDB.push().setValue(new Room(roomID,name,topic,password));
                roomDB.child(roomID).setValue(new Room(roomID,name,topic,password));
                GeneralClass.position = "1";
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                roomDB.child(roomID)
                        .child(GeneralClass.USER_CHILD)
                        .child("1")
                        .child(user.getUid())
                        //.setValue(user.getUid());
                        .setValue(user.getDisplayName());
//                roomDB.child(roomID).child("Create").child(user.getUid())
//                        .setValue(user.getDisplayName());
                roomDB.child(roomID).child(GeneralClass.READY_CHILD).setValue(0);
                if ((Context)activity instanceof HomeActivity || (Context)activity instanceof RoomListActivity){
                    //Intent roomListIntent = new Intent(activity, WaitingRoomActivity.class);
                    Intent intent = new Intent(activity, WaitingRoomActivity.class);
                    intent.putExtra("curRoom",roomID);
                    intent.putExtra("topic",topic);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }
        }
    }



}
