package com.example.beou.a1453028_1453066_final.component;

import com.example.beou.a1453028_1453066_final.R;

/**
 * Created by BeoU on 11/30/2016.
 */

public class User {
    private String name = null;
    private String email = null;
    private String photoUrl = null;
    private String userId = null;


    public User() {
    }

    public User(String name, String email, String photoUrl, String userId) {
        this.name = name;
        this.email = email;
        this.photoUrl = photoUrl;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
