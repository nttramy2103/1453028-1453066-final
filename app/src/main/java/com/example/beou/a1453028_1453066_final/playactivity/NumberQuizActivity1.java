package com.example.beou.a1453028_1453066_final.playactivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beou.a1453028_1453066_final.ChooseTopicActivity;
import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

/**
 * Created by BeoU on 11/29/2016.
 */

public class NumberQuizActivity1 extends Activity {
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    Button btn0,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btnBack,btnEnter;
    TextView txtNumber, txtQuesNum, txtTime;

    ImageView imgQues;
    boolean isQuesExist = true;
    FirebaseDatabase root = FirebaseDatabase.getInstance();
    DatabaseReference curQuesUserRef = root
            .getReference(GeneralClass.USER_CHILD)
            .child(user.getUid())
            .child(GeneralClass.PRACTICE_CHILD)
            .child(GeneralClass.NUMBER_CHILD);
    
    int curQues;

    DatabaseReference quesNode = root
            .getReference(GeneralClass.PRACTICE_CHILD)
            .child(GeneralClass.NUMBER_CHILD);
    int curAnswer;
    
    String curQuesImgLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number_1);
        addControl();
        ////Toast.makeText(this, "", //Toast.LENGTH_SHORT).show();
        //curQues = Integer.parseInt(curQuesUserRef.toString());

        //lắng nghe sự kiện thay đổi câu hỏi ở node user để load câu hỏi phù hợp
        loadQuestion();

        onButtonNumberClick(btn1,"1");
        onButtonNumberClick(btn2,"2");
        onButtonNumberClick(btn3,"3");
        onButtonNumberClick(btn4,"4");
        onButtonNumberClick(btn5,"5");
        onButtonNumberClick(btn6,"6");
        onButtonNumberClick(btn7,"7");
        onButtonNumberClick(btn8,"8");
        onButtonNumberClick(btn9,"9");
        onButtonNumberClick(btn0,"0");
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = txtNumber.getText().toString();
                int len = str.length();
                if (len!=0){
                    txtNumber.setText(str.substring(0,len-1));
                }
            }
        });
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String strUserAns;
                strUserAns = txtNumber.getText().toString();
                if (strUserAns.length()!=0 && strUserAns.length()<9){
                    //TODO: Chuyen kieu int r so sanh voi dap an
                    if (curAnswer == Integer.parseInt(strUserAns)) {
                        //Dung: load cau tiep theo (thay gitri bien dap an va hinh cau hoi text chua so thu tu cau hoi)
                        //TODO: ktra curques++ có tồn tại trên cây k mới thay đổi
                        isQuesExist(curQues+1);
                    }
                    else{
                        //Sai: khong thay doi trang thai xoa text view
                        Vibrator vibrator = (Vibrator) getApplication().getSystemService(Context.VIBRATOR_SERVICE);
                        vibrator.vibrate(500);
                    }
                    txtNumber.setText("");

                }

            }
        });
    }

    /**
     * //lắng nghe sự kiện thay đổi câu hỏi ở node user để load câu hỏi phù hợp
     */
    private void loadQuestion() {
        curQuesUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                curQues = Integer.parseInt(dataSnapshot.getValue().toString());
                //Toast.makeText(NumberQuizActivity1.this,curQues+ "", //Toast.LENGTH_SHORT).show();
                txtQuesNum.setText("Câu "+ curQues+": ");
                getCurAnswer(curQues);
                getQuesContent(curQues);
                getCurImgQues(curQues);
                //Toast.makeText(NumberQuizActivity1.this, "curAnswer: "+curAnswer+"", //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Kiểm tra xem câu hỏi truyền vào có nội dung trên firebase chưa
     * @param ques
     */
    private void isQuesExist(final int ques){
        quesNode.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
              isQuesExist = dataSnapshot.hasChild(ques+"");
                if (dataSnapshot.hasChild(ques+"")) {
                    curQues++;
                    updateCurQuesUser(curQues);
                    txtQuesNum.setText("Câu "+ curQues+":");
                    //getCurAnswer(curQues);
                    //getCurImgQues(curQues);
                }
                else{
                    Toast.makeText(NumberQuizActivity1.this, "Bạn đã hoàn thành hết câu hỏi trong chủ đề này, câu hỏi sẽ được cập nhật trong thời gian tới", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplication(), ChooseTopicActivity.class));
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void onButtonNumberClick(Button button, final String num){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNumber.getText().toString().length()<8) {
                    txtNumber.append(num);
                }
            }
        });
    }

    private void addControl() {
        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnEnter = (Button) findViewById(R.id.btnEnter);
        txtNumber = (TextView) findViewById(R.id.txtNumber);
        txtQuesNum = (TextView) findViewById(R.id.txtQues);
        txtTime = (TextView) findViewById(R.id.txtCountTime);
        imgQues = (ImageView) findViewById(R.id.imgQues);
    }

    /**
     * lấy đáp án của câu hỏi truyền vào trên firebase
     * @param curQues
     */
    public void getCurAnswer(final int curQues) {

        //quesNode.child("1").child("Answer").setValue("9");
        quesNode.child(curQues+"")
                .child(GeneralClass.ANSWER_CHILD)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ////Toast.makeText(NumberQuizActivity1.this, "curQues: "+curQues+"", //Toast.LENGTH_SHORT).show();
                curAnswer = Integer.parseInt(dataSnapshot.getValue().toString());
                //Toast.makeText(NumberQuizActivity1.this, "curAnswer1: "+curAnswer+"", //Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    /**
     * lấy link ảnh của câu hỏi hiện tại trên firebase
     * load lên image view của câu hỏi
     * @param curQues
     */
    public void getCurImgQues(int curQues) {

        quesNode.child(curQues+"")
                .child(GeneralClass.IMAGE_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                curQuesImgLink = dataSnapshot.getValue().toString();
                Picasso.with(getApplication())
                        .load(curQuesImgLink)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error)
                        .into(imgQues);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Lấy nội dung câu hỏi
     * @param curQues
     */
    private void getQuesContent(int curQues) {

        quesNode.child(curQues+"")
                .child(GeneralClass.QUES_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                txtQuesNum.append(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void updateCurQuesUser(int curQues)
    {
        curQuesUserRef.setValue(curQues);
    }
}
