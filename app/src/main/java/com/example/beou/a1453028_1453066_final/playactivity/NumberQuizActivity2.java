package com.example.beou.a1453028_1453066_final.playactivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.ResultActivity;
import com.example.beou.a1453028_1453066_final.ResultItem;
import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by BeoU on 11/29/2016.
 */

public class NumberQuizActivity2 extends Activity {
    CountDownTimer countDownTimer;
    int remainSecond = GeneralClass.MAX_SEC;
    int ansPeriod = GeneralClass.MAX_SEC; // thời điểm tloi câu hỏi
    DatabaseReference userNodeDB = FirebaseDatabase.getInstance().getReference(GeneralClass.USER_CHILD);

    LinearLayout linearLayoutPercentBar;
    int curUserBarWidth = 0;  //width của thanh bar hiển hiển thị điểm số của curUser
    int totalScore = 0;
    int opTotalScore = 0;
    int maxDifference = GeneralClass.numOfRequireQues*100; //khoảng cách điểm số lớn nhất giữa 2 ng
    String curRoomID;
    DatabaseReference curRoomDB;
    ArrayList<Integer> randomQues = new ArrayList<Integer>();
    DatabaseReference userScoreDB;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase root = FirebaseDatabase.getInstance();
    DatabaseReference curQuesUserRef = root
            .getReference(GeneralClass.USER_CHILD)
            .child(user.getUid())
            .child(GeneralClass.QUIZ_CHILD)
            .child(GeneralClass.NUMBER_CHILD);

    int curQues;
    int curIndexQues = 0; //index trong array ranQues
    int curAnswer;

    String curQuesImgLink;
    DatabaseReference quesNode = root
            .getReference(GeneralClass.PRACTICE_CHILD)//TODO: đổi lại khi có bộ câu hỏi cho 2 người
            .child(GeneralClass.NUMBER_CHILD);

    Button btn0,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btnBack,btnEnter;
    TextView txtNumber, txtQues, txtTime;
    TextView txtUserScore, txtOpScore, txtCurUserBar,txtOpUserBar;
    ImageView imageavatar1, imageavatar2;
    int second = 1000;
    ImageView imgQues;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number_quiz2);
        addControl();

        //linearLayoutUserBar.getWidth() = linearLayoutUserBar.getWidth();
        //Toast.makeText(this, linearLayoutPercentBar.getWidth()+"", Toast.LENGTH_SHORT).show();

        curRoomID = getIntent().getStringExtra("roomID");

        countDownTimer = new CountDownTimer(GeneralClass.MAX_SEC*1000, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.d("debug", remainSecond +"");
                txtTime.setText(remainSecond +"");
                remainSecond--;
            }

            public void onFinish() {
                Intent intent = new Intent(getApplication(), ResultActivity.class);
                intent.putExtra("roomID",curRoomID);
                startActivity(intent);
                finish();
            }
        }.start();

        curRoomDB = root.getReference(GeneralClass.PLAY_ROOM_CHILD).child(curRoomID);
        setUserAva();
        userScoreDB = curRoomDB.child(GeneralClass.RECORD_CHILD)
                .child(user.getUid());


        ////Toast.makeText(this, "", //Toast.LENGTH_SHORT).show();
        //curQues = Integer.parseInt(curQuesUserRef.toString());

        ////lắng nghe sự kiện thay đổi câu hỏi ở node user để load câu hỏi phù hợp
       // loadQuestion();

                //lay array cau hoi ve sau do push cau hoi hien tai len
        if (curRoomID!=null) {
            curRoomDB.child(GeneralClass.RANDOMARR_CHILD).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    randomQues.add(Integer.parseInt(dataSnapshot.getValue().toString()));
                    if (randomQues.size()==1){
                        curQues = randomQues.get(0);
                        curQuesUserRef.setValue(curQues);
                        loadQuestion();

                    }
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {}
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }

        onButtonNumberClick(btn1,"1");
        onButtonNumberClick(btn2,"2");
        onButtonNumberClick(btn3,"3");
        onButtonNumberClick(btn4,"4");
        onButtonNumberClick(btn5,"5");
        onButtonNumberClick(btn6,"6");
        onButtonNumberClick(btn7,"7");
        onButtonNumberClick(btn8,"8");
        onButtonNumberClick(btn9,"9");
        onButtonNumberClick(btn0,"0");
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = txtNumber.getText().toString();
                int len = str.length();
                if (len!=0){
                    txtNumber.setText(str.substring(0,len-1));
                }
            }
        });
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String strUserAns;
                strUserAns = txtNumber.getText().toString();
                if (strUserAns.length()!=0 && strUserAns.length()<9){
                    //TODO: Chuyen kieu int r so sanh voi dap an
                    if (curAnswer == Integer.parseInt(strUserAns)) {
                        //Dung: load cau tiep theo (thay gitri bien dap an va hinh cau hoi text chua so thu tu cau hoi)
                        //TODO: ktra curques++ có tồn tại trên cây k mới thay đổi
                        //isQuesExist(curQues+1);
                        UpdateScore(100);

                    }
                    else{
                        Vibrator vibrator = (Vibrator) getApplication().getSystemService(Context.VIBRATOR_SERVICE);
                        vibrator.vibrate(500);
                        //Sai: khong thay doi trang thai xoa text view
                        UpdateScore(-50);
                    }
                    onClickEnterButton();
                    txtNumber.setText("");
                }

            }
        });


        //lang nghe su kien thay doi diem so cua doi phương
        curRoomDB.child(GeneralClass.USER_CHILD)
                .child((3-Integer.parseInt(GeneralClass.position))+"")// vi tri node cua doi thu tren ndode user
                .child(GeneralClass.TOTALSCORE_CHILD)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue()!=null && dataSnapshot.getValue().toString().length()!=0) {
                            opTotalScore = Integer.parseInt(dataSnapshot.getValue().toString());
                            txtOpScore.setText(opTotalScore + "");
                            updateScoreBar();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void updateScoreBar(){

        //userWidth+opWidth = totalWidth
        //userWidth-opWidth = (totalScore-opTotalScore)*1.0f/maxDifference)*totalWidth
        int totalWidth = linearLayoutPercentBar.getWidth();
        int userWidth = (int) ((totalWidth+(((totalScore-opTotalScore)*1.0f/maxDifference)*totalWidth))/2);

        txtCurUserBar.setWidth(userWidth);
        txtOpUserBar.setWidth(totalWidth-userWidth);
    }

    private void UpdateScore(int bonusScore){
        //tính tgian tloi cấu hỏi
        second = ansPeriod- remainSecond;
        ansPeriod = remainSecond;

        userScoreDB.push().setValue(new ResultItem(second,bonusScore));
        totalScore+=bonusScore;
        curRoomDB.child(GeneralClass.USER_CHILD)
                .child(GeneralClass.position)
                .child(GeneralClass.TOTALSCORE_CHILD)
                .setValue(totalScore);
        txtUserScore.setText(totalScore+"");

    }
    private void onClickEnterButton() {
        updateScoreBar();
        //Toast.makeText(this, randomQues.size()+ "", Toast.LENGTH_SHORT).show();
        curIndexQues++;
        if (curIndexQues!= GeneralClass.numOfRequireQues) {
            curQues = randomQues.get(curIndexQues);
            updateCurQuesUser(curQues);
        }
        else{
            countDownTimer.cancel();
            Intent intent = new Intent(getApplication(), ResultActivity.class);
            intent.putExtra("roomID",curRoomID);
            startActivity(intent);
            finish();
        }
    }

    /**
     * //lắng nghe sự kiện thay đổi câu hỏi ở node user để load câu hỏi phù hợp
     */
    private void loadQuestion() {
        curQuesUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                curQues = Integer.parseInt(dataSnapshot.getValue().toString());
                //Toast.makeText(NumberQuizActivity1.this,curQues+ "", //Toast.LENGTH_SHORT).show();
                getCurAnswer(curQues);
                getQuesContent(curQues);
                getCurImgQues(curQues);
                //Toast.makeText(NumberQuizActivity1.this, "curAnswer: "+curAnswer+"", //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void onButtonNumberClick(Button button, final String num){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNumber.getText().toString().length()<8) {
                    txtNumber.append(num);
                }
            }
        });
    }

    private void addControl() {
        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnEnter = (Button) findViewById(R.id.btnEnter);
        txtNumber = (TextView) findViewById(R.id.txtNumber);
        txtUserScore = (TextView) findViewById(R.id.txtUserScore);
        txtOpScore = (TextView) findViewById(R.id.txtOpScore);
        txtCurUserBar = (TextView) findViewById(R.id.txtCurUserBar);
        txtOpUserBar = (TextView) findViewById(R.id.txtOpUserBar);
        txtQues = (TextView) findViewById(R.id.txtQues);
        txtTime = (TextView) findViewById(R.id.txtCountDown);
        imgQues = (ImageView) findViewById(R.id.imgQues);
        linearLayoutPercentBar = (LinearLayout) findViewById(R.id.linearLayoutPercentBar);
        imageavatar2 = (ImageView) findViewById(R.id.imageavatar2);
        imageavatar1 = (ImageView) findViewById(R.id.imageavatar1);

    }

    /**
     * lấy đáp án của câu hỏi truyền vào trên firebase
     * @param curQues
     */
    public void getCurAnswer(final int curQues) {

        //quesNode.child("1").child("Answer").setValue("9");
        quesNode.child(curQues+"")
                .child(GeneralClass.ANSWER_CHILD)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ////Toast.makeText(NumberQuizActivity1.this, "curQues: "+curQues+"", //Toast.LENGTH_SHORT).show();
                        curAnswer = Integer.parseInt(dataSnapshot.getValue().toString());
                        //Toast.makeText(NumberQuizActivity1.this, "curAnswer1: "+curAnswer+"", //Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    /**
     * lấy link ảnh của câu hỏi hiện tại trên firebase
     * load lên image view của câu hỏi
     * @param curQues
     */
    public void getCurImgQues(int curQues) {

        quesNode.child(curQues+"")
                .child(GeneralClass.IMAGE_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                curQuesImgLink = dataSnapshot.getValue().toString();
                Picasso.with(getApplication())
                        .load(curQuesImgLink)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error)
                        .into(imgQues);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Lấy nội dung câu hỏi
     * @param curQues
     */
    private void getQuesContent(int curQues) {

        quesNode.child(curQues+"")
                .child(GeneralClass.QUES_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                txtQues.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void updateCurQuesUser(int curQues)
    {
        curQuesUserRef.setValue(curQues);
    }
    private void setUserAva(){
        userNodeDB.child(user.getUid()).child("photoUrl").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Picasso.with(getApplication())
                        .load(dataSnapshot.getValue().toString())
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error)
                        .into(imageavatar1);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        //set ava cho opUser
        if (GeneralClass.opUserID!=null) {
            userNodeDB.child(GeneralClass.opUserID).child("photoUrl").addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Picasso.with(getApplication())
                            .load(dataSnapshot.getValue().toString())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error)
                            .into(imageavatar2);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }

    }
}
