package com.example.beou.a1453028_1453066_final.playactivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beou.a1453028_1453066_final.ChooseTopicActivity;
import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class ShapeQuizActivity1 extends AppCompatActivity {
    ImageButton btn0,btn1,btn2;
    boolean isQuesExist = true;
    //ArrayList<Integer> choice = new ArrayList<Integer>(Arrays.asList(1, 2, 3));

    //button ở index nào sẽ hiện hình của index đó
    //index 2 luôn là câu tloi
    ArrayList<ImageButton> btnChoice;


    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    TextView txtNumber, txtQuesNum, txtTime;
    FirebaseDatabase root = FirebaseDatabase.getInstance();
    DatabaseReference curQuesUserRef = root
            .getReference(GeneralClass.USER_CHILD)
            .child(user.getUid())
            .child(GeneralClass.PRACTICE_CHILD)
            .child(GeneralClass.SHAPE_CHILD);

    int curQues;

    DatabaseReference quesNode = root
            .getReference(GeneralClass.PRACTICE_CHILD)
            .child(GeneralClass.SHAPE_CHILD);

    String curQuesImgLink;
    ImageView imgQues;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shape_quiz1);

        addControl();


        //lắng nghe sự kiện thay đổi câu hỏi ở node user để load câu hỏi phù hợp
        loadQuestion();

    }

    private void addControl() {
        btn0 = (ImageButton) findViewById(R.id.btn0);
        btn1 = (ImageButton) findViewById(R.id.btn1);
        btn2 = (ImageButton) findViewById(R.id.btn2);
        btnChoice = new ArrayList<ImageButton>(Arrays.asList(btn0, btn1, btn2));

        txtQuesNum = (TextView) findViewById(R.id.txtQues);
        txtTime = (TextView) findViewById(R.id.txtCountTime);
        imgQues = (ImageView) findViewById(R.id.imgQues);
    }

    /**
     * //lắng nghe sự kiện thay đổi câu hỏi ở node user để load câu hỏi phù hợp
     */
    private void loadQuestion() {
        //tron vi tri cau button chon cau tloi

        curQuesUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Random rnd = new Random();
                int seed = rnd.nextInt();
                rnd.setSeed(seed);
                Collections.shuffle(btnChoice,rnd);

                //mang btnChoice se random vi tri xuat hien cua hinh trong 3 button
                //mac dinh btn o index khong la dap an theo tu quy dinh (child 2 tren firebase la dap an)
                btnChoice.get(0).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Vibrator vibrator = (Vibrator) getApplication().getSystemService(Context.VIBRATOR_SERVICE);
                        vibrator.vibrate(500);
                    }
                });
                btnChoice.get(1).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Vibrator vibrator = (Vibrator) getApplication().getSystemService(Context.VIBRATOR_SERVICE);
                        vibrator.vibrate(500);
                    }
                });
                btnChoice.get(2).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isQuesExist(curQues+1);
                    }
                });
                //Collections.shuffle(btnChoice);
                curQues = Integer.parseInt(dataSnapshot.getValue().toString());
                //Toast.makeText(NumberQuizActivity1.this,curQues+ "", //Toast.LENGTH_SHORT).show();
                txtQuesNum.setText("Câu "+ curQues+": ");
                getCurImgQues(curQues);
                getQuesContent(curQues);
                loadAnswerButton(curQues);
                //Toast.makeText(NumberQuizActivity1.this, "curAnswer: "+curAnswer+"", //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * lấy link ảnh của câu hỏi hiện tại trên firebase
     * load lên image view của câu hỏi
     * @param curQues
     */
    private void getCurImgQues(int curQues) {

        quesNode.child(curQues+"")
                .child(GeneralClass.IMAGE_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                curQuesImgLink = dataSnapshot.getValue().toString();
                Picasso.with(getApplication())
                        .load(curQuesImgLink)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error)
                        .into(imgQues);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void loadAnswerButton(int curQues) {
        for (int i = 0; i < 3; i++) {
            final int finalI = i;
            quesNode.child(curQues + "")
                    .child(i+"").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //curQuesImgLink = dataSnapshot.getValue().toString();
                    Picasso.with(getApplication())
                            .load(dataSnapshot.getValue().toString())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error)
                            .into(btnChoice.get(finalI));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    /**
     * Kiểm tra xem câu hỏi truyền vào có nội dung trên firebase chưa
     * @param ques
     */
    private void isQuesExist(final int ques){
        quesNode.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                isQuesExist = dataSnapshot.hasChild(ques+"");
                if (dataSnapshot.hasChild(ques+"")) {
                    curQues++;
                    updateCurQuesUser(curQues);
                    txtQuesNum.setText("Câu "+ curQues+":");
                    //getCurAnswer(curQues);
                    //getCurImgQues(curQues);
                }
                else{
                    Toast.makeText(ShapeQuizActivity1.this, "Bạn đã hoàn thành hết câu hỏi trong chủ đề này, câu hỏi sẽ được cập nhật trong thời gian tới", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplication(), ChooseTopicActivity.class));
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Lấy nội dung câu hỏi
     * @param curQues
     */
    private void getQuesContent(int curQues) {

        quesNode.child(curQues+"")
                .child(GeneralClass.QUES_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                txtQuesNum.append(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void updateCurQuesUser(int curQues)
    {
        curQuesUserRef.setValue(curQues);
    }
}
