package com.example.beou.a1453028_1453066_final.playactivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.ResultActivity;
import com.example.beou.a1453028_1453066_final.ResultItem;
import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class ShapeQuizActivity2 extends AppCompatActivity {
    CountDownTimer countDownTimer;
    int remainSecond = GeneralClass.MAX_SEC;
    int ansPeriod = GeneralClass.MAX_SEC; // thời điểm tloi câu hỏi
    LinearLayout linearLayoutPercentBar;
    int curUserBarWidth = 0;  //width của thanh bar hiển hiển thị điểm số của curUser
    int opTotalScore = 0;
    int maxDifference = GeneralClass.numOfRequireQues*100; //khoảng cách điểm số lớn nhất giữa 2 ng
    int totalScore = 0;
    ImageButton btn0,btn1,btn2;
    String curRoomID = null;
    int second = 1000;// sô giây tloi 1 câu hỏi
    DatabaseReference curRoomDB;
    ArrayList<Integer> randomQues = new ArrayList<Integer>();
    DatabaseReference userScoreDB;
    DatabaseReference userNodeDB = FirebaseDatabase.getInstance().getReference(GeneralClass.USER_CHILD);
    //button ở index nào sẽ hiện hình của index đó
    //index 2 luôn là câu tloi
    ArrayList<ImageButton> btnChoice;

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    TextView txtUserScore, txtOpScore, txtCurUserBar,txtOpUserBar;
    TextView txtNumber, txtQues, txtTime;
    FirebaseDatabase root = FirebaseDatabase.getInstance();
    DatabaseReference curQuesUserRef = root
            .getReference(GeneralClass.USER_CHILD)
            .child(user.getUid())
            .child(GeneralClass.QUIZ_CHILD)
            .child(GeneralClass.SHAPE_CHILD);

    int curQues;
    int curIndexQues = 0; //index trong array ranQues

    DatabaseReference quesNode = root
            .getReference(GeneralClass.PRACTICE_CHILD)//TODO: đổi lại khi có bộ câu hỏi cho 2 người
            .child(GeneralClass.SHAPE_CHILD);

    String curQuesImgLink;
    ImageView imgQues;
    ImageView imageavatar1, imageavatar2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shape_quiz2);
        addControl();
        curRoomID = getIntent().getStringExtra("roomID");


        countDownTimer = new CountDownTimer(GeneralClass.MAX_SEC*1000, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.d("debug", remainSecond +"");
                txtTime.setText(remainSecond +"");
                remainSecond--;
            }

            public void onFinish() {
                Intent intent = new Intent(getApplication(), ResultActivity.class);
                intent.putExtra("roomID",curRoomID);
                startActivity(intent);
                finish();
            }
        }.start();

        curRoomDB = root.getReference(GeneralClass.PLAY_ROOM_CHILD).child(curRoomID);
        setUserAva();
        userScoreDB = curRoomDB.child(GeneralClass.RECORD_CHILD)
                        .child(user.getUid());
        //Toast.makeText(this, curRoomID, Toast.LENGTH_SHORT).show();


        //lay array cau hoi ve sau do push cau hoi hien tai len
        if (curRoomID!=null) {
            curRoomDB.child(GeneralClass.RANDOMARR_CHILD).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    randomQues.add(Integer.parseInt(dataSnapshot.getValue().toString()));
                    if (randomQues.size()==1){
                        curQues = randomQues.get(0);
                        curQuesUserRef.setValue(curQues);
                        loadQuestion();
                    }
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {}
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }
        //lang nghe su kien thay doi diem so cua doi phương
        curRoomDB.child(GeneralClass.USER_CHILD)
                .child((3-Integer.parseInt(GeneralClass.position))+"")// vi tri node cua doi thu tren ndode user
                .child(GeneralClass.TOTALSCORE_CHILD)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue()!=null && dataSnapshot.getValue().toString().length()!=0) {
                            opTotalScore = Integer.parseInt(dataSnapshot.getValue().toString());
                            txtOpScore.setText(opTotalScore + "");
                            updateScoreBar();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void addControl() {
        btn0 = (ImageButton) findViewById(R.id.btn0);
        btn1 = (ImageButton) findViewById(R.id.btn1);
        btn2 = (ImageButton) findViewById(R.id.btn2);
        btnChoice = new ArrayList<ImageButton>(Arrays.asList(btn0, btn1, btn2));
        txtUserScore = (TextView) findViewById(R.id.txtUserScore);
        txtOpScore = (TextView) findViewById(R.id.txtOpScore);
        txtCurUserBar = (TextView) findViewById(R.id.txtCurUserBar);
        txtOpUserBar = (TextView) findViewById(R.id.txtOpUserBar);
        txtQues = (TextView) findViewById(R.id.txtQues);
        txtTime = (TextView) findViewById(R.id.txtCountDown);
        imgQues = (ImageView) findViewById(R.id.imgQues);
        linearLayoutPercentBar = (LinearLayout) findViewById(R.id.linearLayoutPercentBar);
        imageavatar2 = (ImageView) findViewById(R.id.imageavatar2);
        imageavatar1 = (ImageView) findViewById(R.id.imageavatar1);
    }

    /**
     * //lắng nghe sự kiện thay đổi câu hỏi ở node user để load câu hỏi phù hợp
     */
    private void loadQuestion() {
        //tron vi tri cau button chon cau tloi

        curQuesUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Random rnd = new Random();
                int seed = rnd.nextInt();
                rnd.setSeed(seed);
                Collections.shuffle(btnChoice,rnd);

                //mang btnChoice se random vi tri xuat hien cua hinh trong 3 button
                //mac dinh btn o index khong la dap an theo tu quy dinh (child 2 tren firebase la dap an)
                //TODO:sửa lại truyền tham số second tính tgian
                btnChoice.get(0).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Vibrator vibrator = (Vibrator) getApplication().getSystemService(Context.VIBRATOR_SERVICE);
                        vibrator.vibrate(500);
                        UpdateScore(-50);
                        onClickButton();
                    }
                });
                btnChoice.get(1).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Vibrator vibrator = (Vibrator) getApplication().getSystemService(Context.VIBRATOR_SERVICE);

                        vibrator.vibrate(500);
                        UpdateScore(-50);
                        onClickButton();
                    }
                });
                btnChoice.get(2).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //isQuesExist(curQues+1);
                        UpdateScore(100);
                        onClickButton();
                    }
                });

                curQues = Integer.parseInt(dataSnapshot.getValue().toString());
                //Toast.makeText(NumberQuizActivity1.this,curQues+ "", //Toast.LENGTH_SHORT).show();
                //txtQuesNum.setText("Câu "+ curQues+": ");
                getCurImgQues(curQues);
                getQuesContent(curQues);
                loadAnswerButton(curQues);
                //Toast.makeText(NumberQuizActivity1.this, "curAnswer: "+curAnswer+"", //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public void updateScoreBar(){

        //userWidth+opWidth = totalWidth
        //userWidth-opWidth = (totalScore-opTotalScore)*1.0f/maxDifference)*totalWidth
        int totalWidth = linearLayoutPercentBar.getWidth();
        int userWidth = (int) ((totalWidth+(((totalScore-opTotalScore)*1.0f/maxDifference)*totalWidth))/2);

        txtCurUserBar.setWidth(userWidth);
        txtOpUserBar.setWidth(totalWidth-userWidth);
    }

    private void UpdateScore(int bonusScore){
        //tính tgian tloi cấu hỏi
        second = ansPeriod- remainSecond;
        ansPeriod = remainSecond;

        userScoreDB.push().setValue(new ResultItem(second,bonusScore));
        totalScore+=bonusScore;
        curRoomDB.child(GeneralClass.USER_CHILD)
                .child(GeneralClass.position)
                .child(GeneralClass.TOTALSCORE_CHILD)
                .setValue(totalScore);
        txtUserScore.setText(totalScore+"");

    }
    private void onClickButton() {
        //Toast.makeText(this, randomQues.size()+ "", Toast.LENGTH_SHORT).show();
        updateScoreBar();
        curIndexQues++;
        if (curIndexQues!= GeneralClass.numOfRequireQues) {
            curQues = randomQues.get(curIndexQues);
            updateCurQuesUser(curQues);
        }
        else{
            countDownTimer.cancel();
            Intent intent = new Intent(getApplication(), ResultActivity.class);
            intent.putExtra("roomID",curRoomID);
            startActivity(intent);
            finish();
        }
    }

    /**
     * lấy link ảnh của câu hỏi hiện tại trên firebase
     * load lên image view của câu hỏi
     * @param curQues
     */
    private void getCurImgQues(int curQues) {

        quesNode.child(curQues+"")
                .child(GeneralClass.IMAGE_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue()!= null) {
                    curQuesImgLink = dataSnapshot.getValue().toString();
                    Picasso.with(getApplication())
                            .load(curQuesImgLink)
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error)
                            .into(imgQues);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void loadAnswerButton(int curQues) {
        for (int i = 0; i < 3; i++) {
            final int finalI = i;
            quesNode.child(curQues + "")
                    .child(i+"").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //curQuesImgLink = dataSnapshot.getValue().toString();
                    if (dataSnapshot.getValue()!=null) {
                        Picasso.with(getApplication())
                                .load(dataSnapshot.getValue().toString())
                                .placeholder(R.drawable.loading)
                                .error(R.drawable.error)
                                .into(btnChoice.get(finalI));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }


    /**
     * Lấy nội dung câu hỏi
     * @param curQues
     */
    private void getQuesContent(int curQues) {

        quesNode.child(curQues+"")
                .child(GeneralClass.QUES_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                txtQues.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void updateCurQuesUser(int curQues)
    {
        curQuesUserRef.setValue(curQues);
    }
    private void setUserAva(){
        userNodeDB.child(user.getUid()).child("photoUrl").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Picasso.with(getApplication())
                        .load(dataSnapshot.getValue().toString())
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error)
                        .into(imageavatar1);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        //set ava cho opUser
        if (GeneralClass.opUserID!=null) {
            userNodeDB.child(GeneralClass.opUserID).child("photoUrl").addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Picasso.with(getApplication())
                            .load(dataSnapshot.getValue().toString())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error)
                            .into(imageavatar2);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }

    }
}
