package com.example.beou.a1453028_1453066_final.useractivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.beou.a1453028_1453066_final.HomeActivity;
import com.example.beou.a1453028_1453066_final.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LogInActivity extends AppCompatActivity {
    EditText txtEmail,txtPassword;
    Button btnLogin,btnSignup,btnResetPass;
    ProgressBar progressBar;
    private FirebaseAuth mAuth;
    //private FirebaseAuth.AuthStateListener mAuthListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        addControl();
        mAuth = FirebaseAuth.getInstance();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpActivity = new Intent(getApplication(),SignUpActivity.class);
                startActivity(signUpActivity);
                finish();
            }
        });
        btnResetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resetPassActivity = new Intent(getApplication(),ResetPasswordActivity.class);
                startActivity(resetPassActivity);
                //finish();
            }
        });

    }
    private void informErrorEditText(EditText editText,String error){
        editText.setFocusable(true);
        if(editText.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        editText.setError(error);
    }
    private void Login()
    {
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();
        if (email.length() == 0){
            informErrorEditText(txtEmail,"Bạn chưa nhập Email");
            return;
        }
        if (password.length() == 0){
            informErrorEditText(txtPassword,"Bạn chưa nhập Password");
            return;
        }
        else if (password.length() < 6){
            informErrorEditText(txtPassword,"Password yêu cầu 6 kí tự trở lên");
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                    if (!task.isSuccessful()) {
                        //Log.w(TAG, "signInWithEmail:failed", task.getException());
                        Toast.makeText(LogInActivity.this, "signInWithEmail:failed"+task.getException().toString(), Toast.LENGTH_SHORT).show();
                    }else{
                        Intent intent = new Intent(LogInActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    }
                });
    }

    private void addControl() {
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSignup = (Button) findViewById(R.id.btnSignup);
        btnResetPass = (Button) findViewById(R.id.btnResetPass);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }
}
