package com.example.beou.a1453028_1453066_final.useractivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.beou.a1453028_1453066_final.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPasswordActivity extends Activity {
    EditText txtEmail;
    Button btnResetPass, btnLinkToLogin;
    FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        addControl();
        mAuth = FirebaseAuth.getInstance();
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginActivity = new Intent(getApplication(),LogInActivity.class);
                startActivity(loginActivity);
                finish();
            }
        });
        btnResetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = txtEmail.getText().toString();
                if (email.length() == 0){
                    informErrorEditText(txtEmail,"Bạn chưa nhập Email");
                    return;
                }
                mAuth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ResetPasswordActivity.this, "Đã gửi email xác nhận", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }
    private void informErrorEditText(EditText editText,String error){
        editText.setFocusable(true);
        if(editText.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        editText.setError(error);
    }
    private void addControl() {
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLogin);
        btnResetPass = (Button) findViewById(R.id.btnResetPass);

    }
}
