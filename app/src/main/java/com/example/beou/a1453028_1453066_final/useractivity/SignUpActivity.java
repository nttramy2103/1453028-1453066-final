package com.example.beou.a1453028_1453066_final.useractivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.component.User;
import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    //private DatabaseReference mDatabase;
    DatabaseReference myUser;
    //
    EditText txtEmail,txtPassword, txtUsername;
    Button btnSignup,btnLinkToLogin;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        addControl();
        mAuth = FirebaseAuth.getInstance();
        myUser = FirebaseDatabase.getInstance().getReference(GeneralClass.USER_CHILD);

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });

        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginActivity = new Intent(getApplication(),LogInActivity.class);
                startActivity(loginActivity);
                finish();
            }
        });
    }

    private void informErrorEditText(EditText editText,String error){
        editText.setFocusable(true);
        if(editText.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        editText.setError(error);
    }
    private void signUp()
    {
        final String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();
        final String username = txtUsername.getText().toString();
        if (email.length() == 0){
            informErrorEditText(txtEmail,"Bạn chưa nhập Email");
            return;
        }
        if (username.length() == 0){
            informErrorEditText(txtEmail,"Bạn chưa nhập Username");
            return;
        }
        if (password.length() == 0){
            informErrorEditText(txtPassword,"Bạn chưa nhập Password");
            return;
        }
        else if (password.length() < 6){
            informErrorEditText(txtPassword,"Password yêu cầu 6 kí tự trở lên");
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(txtEmail.getText().toString(), txtPassword.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplication(), "Đăng kí thành công", Toast.LENGTH_SHORT).show();

                            final String imgUrl = "https://lh3.googleusercontent.com/proxy/1KeW7l9vJkF7xqdkEGj7lUD3SF6euqRWWGc0_QzLz16cdUDT2uBxyNwetOHOAtXHRKrseeZct7AQ5J9DnE2w0bkb5kajoHaKkxQnefKRLC0yBgzhaMbZWXoCYY-QxNVX5ILPQCggYy0TpuQ=w506-h284";
                            final String userId = task.getResult().getUser().getUid();
                            //đăng kí tên user
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(username)
                                    .setPhotoUri(Uri.parse(imgUrl))
                                    .build();

                            task.getResult().getUser().updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(SignUpActivity.this, "User profile updated.", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                            myUser.child(userId).setValue(new User(username,email,imgUrl,userId));
                            myUser.child(userId).child(GeneralClass.PRACTICE_CHILD).child(GeneralClass.NUMBER_CHILD).setValue(1);
                            myUser.child(userId).child(GeneralClass.PRACTICE_CHILD).child(GeneralClass.SHAPE_CHILD).setValue(1);
                            startActivity(new Intent(getApplication(),LogInActivity.class));
                            finish();
                        }
                        else
                        {
                            Toast.makeText(getApplication(), "Đăng kí thất bại do nhập sai thông tin hoặc email đã được sử dụng"
                                    , Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void putUser2DB(User user) {
        myUser.child(user.getUserId()).setValue(user);
    }

    private void addControl() {
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnSignup = (Button) findViewById(R.id.btnSignup);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLogin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtUsername = (EditText) findViewById(R.id.txtUsername);
    }
}
