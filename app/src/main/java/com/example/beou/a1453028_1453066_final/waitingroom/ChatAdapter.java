package com.example.beou.a1453028_1453066_final.waitingroom;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.beou.a1453028_1453066_final.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BeoU on 12/13/2016.
 */

public class ChatAdapter extends ArrayAdapter<ChatItem>{
    Context context;
    String curUserID = FirebaseAuth.getInstance().getCurrentUser().getUid();
    ArrayList<ChatItem> chatItems;
    public ChatAdapter(Context context, int resource, List<ChatItem> objects) {
        super(context, resource, objects);
        this.context = context;
        this.chatItems= (ArrayList<ChatItem>) objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ChatItem chatItem = chatItems.get(position);
        //if (convertView == null) {
            if (curUserID.equals(chatItem.getUserID())) {
                convertView = LayoutInflater.from(context).inflate(R.layout.myuser_item, parent, false);
            }
            else{
                convertView = LayoutInflater.from(context).inflate(R.layout.opuser_item, parent, false);
            }
        //}
        TextView txtMes = (TextView) convertView.findViewById(R.id.txtMes);
        txtMes.setText(chatItem.getMes());
        return convertView;
    }
}
