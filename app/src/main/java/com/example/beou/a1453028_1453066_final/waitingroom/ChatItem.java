package com.example.beou.a1453028_1453066_final.waitingroom;

/**
 * Created by BeoU on 12/13/2016.
 */

public class ChatItem {
    String userID;
    String mes;

    public ChatItem() {
    }

    public ChatItem(String userID, String mes) {
        this.userID = userID;
        this.mes = mes;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }


    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

}
