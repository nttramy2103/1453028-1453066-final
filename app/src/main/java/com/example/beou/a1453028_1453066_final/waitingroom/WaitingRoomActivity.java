package com.example.beou.a1453028_1453066_final.waitingroom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.example.beou.a1453028_1453066_final.playactivity.NumberQuizActivity2;
import com.example.beou.a1453028_1453066_final.playactivity.ShapeQuizActivity2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;

public class WaitingRoomActivity extends Activity {

    Button btnSend, btnReady;
    EditText txtComment;
    ListView listViewChat;
    DatabaseReference curRoomDB;
    DatabaseReference mesDB;
    DatabaseReference quizDB = null;
    DatabaseReference userNodeDB = FirebaseDatabase.getInstance().getReference(GeneralClass.USER_CHILD);
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    ArrayList<ChatItem> chatItems = new ArrayList<ChatItem>();
    String cmt;
    String topic;
    int numQues=0;
    int numReady = 0;
    ImageView imageavatar2, imageavatar1;
    ArrayList<Integer> ranQues = new ArrayList<Integer>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_room);
        addControl();
        final ChatAdapter chatAdapter = new ChatAdapter(this,R.layout.myuser_item,chatItems);
        listViewChat.setAdapter(chatAdapter);
        btnSend.setEnabled(false);
        btnReady.setEnabled(false);
        final String curRoom = getIntent().getStringExtra("curRoom");
        //Toast.makeText(this, curRoom, Toast.LENGTH_SHORT).show();
        curRoomDB = FirebaseDatabase.getInstance().getReference(GeneralClass.PLAY_ROOM_CHILD).child(curRoom);
        //FirebaseDatabase.getInstance().getReference(GeneralClass.ROOM_CHILD).child(curRoom).child("numOfUser").addValueEventListener(new ValueEventListener() {

        //set avatar của user
        setMyUserAva();

        //lấy topic
        topic =getIntent().getStringExtra("topic");
        if (topic.equals(GeneralClass.topic[0])){
            quizDB = FirebaseDatabase
                    .getInstance()
                    .getReference(GeneralClass.PRACTICE_CHILD) //TODO: sửa lại khi có bộ câu hỏi bên child Quiz
                    .child(GeneralClass.SHAPE_CHILD);
            if (quizDB!=null) {
                GetNumQues();
            }

        }
        //nhập số
        else if (topic.equals(GeneralClass.topic[1])){
            quizDB = FirebaseDatabase
                    .getInstance()
                    .getReference(GeneralClass.PRACTICE_CHILD) //TODO: sửa lại khi có bộ câu hỏi bên child Quiz
                    .child(GeneralClass.NUMBER_CHILD);
            if (quizDB!=null) {
                GetNumQues();
            }
        }



        //Toast.makeText(this, topic, Toast.LENGTH_SHORT).show();
        //khi du 2 nguoi thi enable btn send
        curRoomDB.child("numOfUser").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!= null) {
                    if (dataSnapshot.getValue().toString().equals("2")) {
                        btnSend.setEnabled(true);
                        btnReady.setEnabled(true);
                        //set ava của đối thủ
                        setOpUserAva();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //chat feature ------------------------------------------------------------
        mesDB = curRoomDB.child(GeneralClass.MESSAGES_CHILD);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cmt=txtComment.getText().toString();
                if (cmt.length()!= 0){
                    txtComment.setText("");
                    mesDB.push().setValue(new ChatItem(user.getUid(),cmt));;
//                    mesDB.child(key).child("ID").setValue(user.getUid());
//                    mesDB.child(key).child("Message").setValue(cmt);
                }
            }
        });

        //hien tin nhan len tren list view
        mesDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                chatItems.add(dataSnapshot.getValue(ChatItem.class));
                //rooms.add((Room) dataSnapshot.getValue());
                chatAdapter.notifyDataSetChanged();
                listViewChat.setSelection(listViewChat.getAdapter().getCount()-1);
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        //-------------------------------------------------------------------------

        //xét trường hợp ready
        curRoomDB.child(GeneralClass.READY_CHILD).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    numReady = Integer.parseInt(dataSnapshot.getValue().toString());
                    //Toast.makeText(WaitingRoomActivity.this, numReady + "", Toast.LENGTH_SHORT).show();
                    if (numReady == 2) {
                        //TODO: Gửi array câu hỏi ngẫu nhiên đã tạo -DONE
                        if (GeneralClass.position.equals("1")) {
                            //TODO: Sửa lại add ques chỗ này -DONE
                            //Toast.makeText(WaitingRoomActivity.this, numQues+ "", Toast.LENGTH_SHORT).show();
                            if (ranQues.size()!=numQues) {
                                for (int i = 1; i <= numQues; i++) {
                                    ranQues.add(i);
                                }
                                Collections.shuffle(ranQues);

                            }
                            curRoomDB.child(GeneralClass.RANDOMARR_CHILD)
                                    .setValue(ranQues.subList(0,GeneralClass.numOfRequireQues));
                        //Toast.makeText(WaitingRoomActivity.this, ranQues.subList(0, GeneralClass.numOfRequireQues).toString(), Toast.LENGTH_SHORT).show();
                        }
                        if (topic.equals(GeneralClass.topic[0])) {
                            Intent intent = new Intent(getApplication(), ShapeQuizActivity2.class);
                            intent.putExtra("roomID", curRoom);
                            startActivity(intent);
                            finish();
                        }
                        else if (topic.equals(GeneralClass.topic[1])){
                            Intent intent = new Intent(getApplication(), NumberQuizActivity2.class);
                            intent.putExtra("roomID", curRoom);
                            startActivity(intent);
                            finish();
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btnReady.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numReady++;
                curRoomDB.child(GeneralClass.READY_CHILD).setValue(numReady);
                btnReady.setEnabled(false);
            }
        });


    }

    private void GetNumQues() {
        //Toast.makeText(this, "abc", Toast.LENGTH_SHORT).show();
        quizDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot!= null) {
                    numQues++;
                   // Toast.makeText(WaitingRoomActivity.this, dataSnapshot.getValue().toString(), Toast.LENGTH_SHORT).show();

                    //Toast.makeText(getApplication(), numQues+"", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });
    }

    private void addControl() {
        txtComment = (EditText) findViewById(R.id.txtComment);
        btnSend = (Button) findViewById(R.id.btnSend);
        btnReady = (Button) findViewById(R.id.btnReady);
        listViewChat = (ListView) findViewById(R.id.listViewChat);
        imageavatar2 = (ImageView) findViewById(R.id.imageavatar2);
        imageavatar1 = (ImageView) findViewById(R.id.imageavatar1);
    }

    private void setMyUserAva(){
        userNodeDB.child(user.getUid()).child("photoUrl").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Picasso.with(getApplication())
                        .load(dataSnapshot.getValue().toString())
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error)
                        .into(imageavatar1);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });




    }
    private void setOpUserAva(){
        //lấy userID của opUser, set ava của opuser
        curRoomDB.child(GeneralClass.USER_CHILD)
                .child((3-Integer.parseInt(GeneralClass.position))+"")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        if(dataSnapshot.getKey().equals(GeneralClass.TOTALSCORE_CHILD) == false){
                            //Toast.makeText(ResultActivity.this, dataSnapshot.getKey(), Toast.LENGTH_SHORT).show();
                            GeneralClass.opUserID = dataSnapshot.getKey();
                            userNodeDB.child(dataSnapshot.getKey()).child("photoUrl").addValueEventListener(new ValueEventListener() {

                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Picasso.with(getApplication())
                                            .load(dataSnapshot.getValue().toString())
                                            .placeholder(R.drawable.loading)
                                            .error(R.drawable.error)
                                            .into(imageavatar2);
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                    }
                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {}
                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
    }
}
