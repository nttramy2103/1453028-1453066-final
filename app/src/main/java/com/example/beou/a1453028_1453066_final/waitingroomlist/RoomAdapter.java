package com.example.beou.a1453028_1453066_final.waitingroomlist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.example.beou.a1453028_1453066_final.component.Room;

import java.util.ArrayList;

/**
 * Created by BeoU on 12/1/2016.
 */

public class RoomAdapter extends ArrayAdapter<Room> {
    Context context;
    ArrayList<Room> rooms;
    public RoomAdapter(Context context, int resource, ArrayList<Room> objects) {
        super(context, resource, objects);
        this.context = context;
        rooms = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.room_item,parent,false);
        }
        ImageView imgKey = (ImageView) convertView.findViewById(R.id.imgKey);
        ImageView imgRoom = (ImageView) convertView.findViewById(R.id.imgRoom);
        TextView txtTopic = (TextView) convertView.findViewById(R.id.txtTopic);
        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);

        Room room = rooms.get(position);
        String topic = room.getTopic();
        txtTopic.setText("Chủ đề: " + topic);
        txtName.setText("Tên phòng: " + room.getName());
        for (int i = 0; i<GeneralClass.topic.length;i++){
            if (topic.equals(GeneralClass.topic[i])){
                imgRoom.setImageResource(GeneralClass.imgTopic[i]);
            }
        }
        if (room.getPassword() != null) {
            if (room.getPassword().length()!=0){
                imgKey.setImageResource(R.drawable.key);
            }
        }


        return convertView;
    }
}
