package com.example.beou.a1453028_1453066_final.waitingroomlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.beou.a1453028_1453066_final.R;
import com.example.beou.a1453028_1453066_final.component.Room;
import com.example.beou.a1453028_1453066_final.waitingroom.WaitingRoomActivity;
import com.example.beou.a1453028_1453066_final.component.GeneralClass;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class RoomListActivity extends Activity {
    ListView listView;
    Button btnCreateRoom;
    ArrayList<Room> rooms = new ArrayList<Room>();
    FirebaseDatabase root;
    DatabaseReference roomDB;
    DatabaseReference playroomDB;
    Room curRoom;
    int count = 0;
    FirebaseUser curUser = FirebaseAuth.getInstance().getCurrentUser();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_list);

        listView = (ListView) findViewById(R.id.listView);
        root= FirebaseDatabase.getInstance();
        roomDB= root.getReference(GeneralClass.ROOM_CHILD);
        playroomDB= root.getReference(GeneralClass.PLAY_ROOM_CHILD);


        final RoomAdapter roomAdapter = new RoomAdapter(this,R.layout.room_item,rooms);
        listView.setAdapter(roomAdapter);

        btnCreateRoom = (Button) findViewById(R.id.btnCreateRoom);
        btnCreateRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Room room = new Room();
                room.createRoom(RoomListActivity.this);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(RoomListActivity.this, "abc", Toast.LENGTH_SHORT).show();
                curRoom = rooms.get(position);
                if (curRoom.getNumOfUser()==1){
                    roomDB.child(curRoom.getRoomID()).child("numOfUser").setValue(2);
                    //roomDB.child(curRoom.getRoomID()).child(GeneralClass.USER_CHILD).setValue(curUser.getUid());
                    //view.setVisibility(View.INVISIBLE);
                    GeneralClass.position = "2";
                    roomDB.child(curRoom.getRoomID())
                            .child(GeneralClass.USER_CHILD)
                            .child("2")
                            .child(curUser.getUid())
                            .setValue(curUser.getDisplayName());
                            //.setValue(curUser.getUid());


                    //chuyen room qua node play room
                    roomDB.child(curRoom.getRoomID()).removeValue();
                    Intent intent = new Intent(getApplication(),WaitingRoomActivity.class);
                    intent.putExtra("curRoom",curRoom.getRoomID());
                    intent.putExtra("topic",curRoom.getTopic());
                    startActivity(intent);
                    finish();

                }


            }
        });
        roomDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("abc",dataSnapshot.toString());
                Log.d("abcChild",dataSnapshot.getChildren().toString());
                Log.d("abcValue",dataSnapshot.getValue().toString());
                //TODO:xuly numOFUser
                rooms.add(dataSnapshot.getValue(Room.class));
                //rooms.add((Room) dataSnapshot.getValue());
                roomAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, final String s) {
                //Toast.makeText(RoomListActivity.this, dataSnapshot.getValue().toString(), Toast.LENGTH_SHORT).show();
                //Toast.makeText(RoomListActivity.this, dataSnapshot.getChildren().toString(), Toast.LENGTH_SHORT).show();
                //dataSnapshot.getChildren().toString();
                //Toast.makeText(RoomListActivity.this, "sTRING"+s, Toast.LENGTH_LONG).show();
                //Toast.makeText(RoomListActivity.this, "kEY"+dataSnapshot.getKey(), Toast.LENGTH_LONG).show();
                //dataSnapshot.getKey();
//                final String roomId = dataSnapshot.getKey();
//                roomDB.child(roomId).child("numOfUser").addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String numOfUserStr = dataSnapshot.getValue().toString();
//                        if (numOfUserStr.equals("2")){
//                            removeRoomFromList(s);
//                        }
//                        else if(numOfUserStr.equals("0")){
//                            removeRoomFromList(s);
//                        }
//                        else if (numOfUserStr.equals("1")){
//                            roomDB.child(roomId).child("isWaiting").addValueEventListener(new ValueEventListener() {
//                                @Override
//                                public void onDataChange(DataSnapshot dataSnapshot) {
//                                    dataSnapshot.getValue()
//                                }
//
//                                @Override
//                                public void onCancelled(DatabaseError databaseError) {
//
//                                }
//                            });
//                        }
//                        roomAdapter.notifyDataSetChanged();
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                playroomDB.child(dataSnapshot.getKey()).setValue(dataSnapshot.getValue());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

//    private void removeRoomFromList(String roomId) {
//        for (int i=0; i<rooms.size();i++){
//            if(roomId.equals(rooms.get(i).getRoomID())){
//                rooms.remove(i);
//                break;
//            }
//        }
//    }
}
